/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages

        $(document).foundation(); // Foundation JavaScript
        
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.





// ====================== smooth scroll anchor ================

  // jQuery(function() {
  //   jQuery('.scrolldown').click(function() {
  //     if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') && location.hostname === this.hostname) {
  //       var target = jQuery(this.hash);
  //       target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
  //       if (target.length) {
  //         jQuery('html,body').animate({
  //           scrollTop: target.offset().top - 80
  //         }, 1000);
  //         return false;
  //       } 
  //     }
  //   });
  // });

// ====================== end smooth scroll anchor ================ 



jQuery(document).ready(function(){      // global document.ready

	
	
	$('.top-line-text').slick({
		arrows:false,
		fade:true,
		autoplay: true,
		autoplaySpeed: 9000,
	})
		
	$(".top-line-lang li a").click(function(){
		$(".top-line-lang li a").removeClass('active')
		$(this).addClass("active")
	})
	
	 $('[data-trigger]').click(function(){
		var collection =  $(this).data('collection');
		var triger = $(this).data('trigger');
		$('.tab').removeClass('tab-active');
		$(this).addClass('tab-active');
		$('[data-collection="'+collection+'"]').removeClass('active') 
		$('[data-collection="'+collection+'"][data-target="'+triger+'"]').addClass('active')
	});
	
	$('.content-menu a').click(function(e){
		e.preventDefault();
	})
	
	$(window).scroll(function() {
		if ($(this).scrollTop() > 1){  
			$('.header__top').addClass("fix");
			$('#header-menu').addClass("fix");
			$('.header__bottom').addClass("fix");
			
		}
		else{
			$('.header__top').removeClass("fix");
			$('#header-menu').removeClass("fix");
			$('.header__bottom').removeClass("fix");
		}
	})
	
	
	
	
	$('.slider-home').slick({
		centerMode: true,
		centerPadding: '100px',
		slidesToShow: 1,
		autoplay: true,
		autoplaySpeed: 7000,
		nextArrow: $('.next'),
		prevArrow: $('.prew'),
		responsive: [
			
			{
				breakpoint:1024,
			    settings: {
					centerMode: false,
			    }
			},
			{
			    breakpoint: 640,
			    settings: {
					centerMode: false,
			    }
			}
		  ]
	});
	$('.reviews-slider').slick({
		slidesToShow: 3,
		adaptiveHeight:true,
		nextArrow: $('.reviews-next'),
		prevArrow: $('.reviews-prew'),
		responsive: [
			
			{
			    breakpoint: 1024,
			    settings: {
					slidesToShow: 1,
				    centerMode: true,
				    centerPadding: '150px',
			    }
			},
			{
				breakpoint: 600,
			    settings: {
					slidesToShow: 1,
			    }
			}
		  ]
	});
 
	
	$( ".icon" ).click(function(){
		$(this).toggleClass('active');
		$('#header-menu').toggleClass('active');
	});
	
	$(document).mouseup(function (e) {
		var container = $("#header-menu");
		if (container.has(e.target).length === 0){
			container.removeClass('active');
			$( ".icon" ).removeClass('active')
		}
	});
	
	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = 'https://connect.facebook.net/uk_UA/sdk.js#xfbml=1&version=v2.12';
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'))
});

jQuery(document).ready(function(){  
 // wordpress admin panel 
    jQuery('html').attr('style', 'margin-top: 0!important');
    jQuery('#wpadminbar').addClass('overflow');
    var hide;
    jQuery('#wpadminbar').on('mouseover', function(){
      setTimeout(function(){
        jQuery('#wpadminbar').removeClass('overflow');
      },1000);
      if(!jQuery('#wpadminbar').hasClass('open')){
        jQuery('#wpadminbar').addClass('open overflow');
      } else{
        clearTimeout(hide);
      }
    });
    jQuery('#wpadminbar').on('mouseleave', function(){
      hide = setTimeout(function(){
        jQuery('#wpadminbar').addClass('overflow');
        jQuery('#wpadminbar').removeClass('open');
      },2000);
    });
  // end wordpress admin panel
});






